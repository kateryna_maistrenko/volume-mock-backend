#Oy vse

Для начала нужно установить [node](https://nodejs.org/en/) посвежее;
Затем установить необходимые пакеты внутри проекта

```
npm i
```

##Server
Чтобы запустить [сервер](http://localhost:8080) выполните команду

```
npm start
```

# How to use
- Decorator role-based security. Scopes: controller and method.
    We could work with JWT tokens. Decrypt token, take authorities from token
    and check if user has an authority
    Example:
    ```typescript
    @Secured('ADMIN,USER')
    @Path('/hello')
    export class HelloController {
        /**
         * Send a greeting message.
         * @param name The name that will receive our greeting message
         */
        @Secured('ADMIN')
        @Path(':name')
        @GET
        sayHello(@PathParam('name') name: string): string {
            return 'Hello ' + name;
        }
    }
    ```

## Swagger Docs Generation

```
npm run swagger
```

## Generate Docs

```
npm run doc
```

The project documentation will be saved under ```./doc``` folder.
