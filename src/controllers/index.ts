import {HelloController} from './example';
import {CategoryController} from './category/category.controller';

export default [
    HelloController,
    CategoryController,
];
