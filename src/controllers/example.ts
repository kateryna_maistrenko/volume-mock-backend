import express from 'express';
import {ContextRequest, ContextResponse, GET, Path, PathParam} from 'typescript-rest';

@Path('/hello')
export class HelloController {

    /**
     * Test url: `http://localhost:8080/hello/world/eos`
     * @param {string} name
     * @param {e.Response} res
     * @param {e.Request} req
     * @returns {Promise<string>}
     */
    @Path('world/:name')
    @GET
    async sayHello(@PathParam('name') name: string, @ContextResponse res: express.Response, @ContextRequest req: express.Request) {
        console.log(req.body);
        console.log(res);
        console.log(name);
        const response = await new Promise((resolve) => {
            setTimeout(() => {
                resolve(`${name[0].toUpperCase()}${name.slice(1)}`);
            }, 200);
        });
        return `Hello ${response}`;
    }
}
