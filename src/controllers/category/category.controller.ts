import {GET, Path} from 'typescript-rest';

@Path('api/category')
export class CategoryController {
    @GET
    getPost() {
        return [
            {id: 0, name: 'other'},
            {id: 1, name: 'cars'},
            {id: 2, name: 'cats'},
        ];
    }
}
