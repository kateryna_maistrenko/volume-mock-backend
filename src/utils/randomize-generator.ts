import {imageUrls, Sentences, Titles} from "./text.constants";

export class RandomizeGenerator {

    static getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    static get randomWord() {
        return Titles[RandomizeGenerator.getRandomInt(Titles.length - 1)]
    }

    static get randomShortString() {
        let phrase = '';
        const length = RandomizeGenerator.getRandomInt(8) || 1;
        for (let i = 0; i < length; i++) {
            phrase += RandomizeGenerator.randomWord + (i === length - 1 ? '' : ' ')
        }
        return phrase
    }

    static get randomImage() {
        return imageUrls[RandomizeGenerator.getRandomInt(imageUrls.length - 1)]
    }

    static get randomLongString() {
        return Sentences[RandomizeGenerator.getRandomInt(Sentences.length - 1)]
    }

    static get randomNumber() {
        return RandomizeGenerator.getRandomInt(500)
    }

    static get randomArrayOfShortString() {
        const array = [];
        const length = RandomizeGenerator.getRandomInt(10);
        for (let i = 0; i < length; i++) {
            array.push(RandomizeGenerator.randomWord)
        }
        return array
    }

    static get randomArrayOfNumbers() {
        const array = [];
        const length = RandomizeGenerator.getRandomInt(10);
        for (let i = 0; i < length; i++) {
            array.push(RandomizeGenerator.randomNumber)
        }
        return array
    }
}