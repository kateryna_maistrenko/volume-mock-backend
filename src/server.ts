import express = require('express');
import * as bodyParser from 'body-parser';
import controllers from './controllers';
import {Server as TSServer} from 'typescript-rest';
import morgan = require('morgan');
import compression = require('compression');
import cors = require('cors');
import * as path from 'path';

export class Server {

    public static port = '8080';

    // set app to be of type express.Application
    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        TSServer.useIoC();
        TSServer.buildServices(this.app, ...controllers);
    }

    // application config
    public config(): void {
        // express middleware
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(morgan('dev'));
        this.app.use(compression());

        // @ts-ignore
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', 'http://localhost:' + Server.port);
            res.header('Access-Control-Allow-Origin', 'http://localhost:3100');
            res.header(
                'Access-Control-Allow-Methods',
                'GET, POST, PUT, DELETE, OPTIONS',
            );
            res.header(
                'Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials',
            );
            res.header('Access-Control-Allow-Credentials', 'true');
            next();
        });

        this.app.use(express.static(path.join(__dirname, 'public'), {maxAge: 31557600000}));
        this.app.use(cors());
    }

}

// export
export default new Server().app;
